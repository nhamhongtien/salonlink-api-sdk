import { IUser } from "../user/models";
import { PlatForm } from "../../type";

export interface INotificationCategory {
    id: string;
    name: string;
}

export interface INotification {
    id: string;
    verb: string;
    title: string;
    content: string;
    created: string;
    read: string;
    actor?: IUser;
    category?: INotificationCategory;
}

export interface INotificationDevice {
    id: number;
    name?: string;
    registration_id: string;
    device_id: string;
    type?: PlatForm;
    date_created?: Date | string;
}
