import { IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter } from './adapter';
export interface IJwtToken {
    refresh: string;
    access: string;
}
export declare class JwtAuth extends AbstractAuthAdapter {
    login(username: string, password: string): Promise<IJwtToken>;
    getAuthToken(): Promise<IAuthToken | null>;
    refreshToken(): Promise<void>;
}
