export declare class SocketEvent {
    name: string;
    payload?: {
        [key: string]: any;
    } | undefined;
    constructor(name: string, payload?: {
        [key: string]: any;
    } | undefined);
}
export interface ISocketEventListener {
    /**
     * @param event SocketEvent
     */
    (event: SocketEvent): Promise<void>;
}
export declare class SocketIO {
    private url;
    private static _instance?;
    private socket;
    private events;
    private constructor();
    static instance(url: string): SocketIO;
    connect(jwt: string): void;
    get id(): string | null;
    get connected(): boolean;
    isConnected(): boolean;
    close(): void;
    disconnect(): void;
    on(eventName: string, listener: ISocketEventListener): void;
    emit(eventName: string, payload: any): this;
    /**
     * Add an event handler for given eventName
     *
     * @param {string} eventName
     * @param {function|object} listener a function or an instance of ISocketEventListener
     */
    addEventListener(eventName: string, listener: ISocketEventListener): this;
    private handleEvent;
}
