"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../../http/client");
const formDataHeaders = { 'Content-Type': 'multipart/form-data' };
class User {
    static register(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return client_1.ApiClient.post('/auth/registration/', params, {}, false);
        });
    }
    static me() {
        return __awaiter(this, void 0, void 0, function* () {
            return client_1.ApiClient.get('/users/me/');
        });
    }
    static profile() {
        return __awaiter(this, void 0, void 0, function* () {
            return client_1.ApiClient.get('/profile/');
        });
    }
    static updateProfile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = '/users/profile/';
            return client_1.ApiClient.patch(uri, data, {});
        });
    }
    static existsUser(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/users/exists/`;
            const r = yield client_1.ApiClient.get(uri, { username });
            return r.exists;
        });
    }
    static existsEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/users/exists/`;
            const r = yield client_1.ApiClient.get(uri, { email });
            return r.exists;
        });
    }
    static updateEmail(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/users/email/change/`;
            return client_1.ApiClient.patch(uri, params);
        });
    }
}
exports.User = User;
