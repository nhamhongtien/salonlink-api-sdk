import { ApiClient, IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter, ISocalAuth, SocialLoginParams } from './adapter';
import * as utils from '../../../utils';

export interface IToken {
    key: string;
}

export class TokenAuth extends AbstractAuthAdapter implements ISocalAuth {

    async login(username: string, password: string): Promise<IToken> {
        const data = { email: username, password };
        const r = await ApiClient.post('/auth/login/', data, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async getAuthToken(): Promise<IAuthToken | null> {
        const config = ApiClient.getApiConfig();
        const json: string = await config.session.get(config.AUTH_SESSION_KEY);
        const token = utils.tryParseJson(json) as IToken;
        if (!token || !token.key) {
            return null;
        }
        return {
            token_type: 'Bearer',
            access_token: token.key
        };
    }

    async refreshToken(): Promise<void> { }

    async facebookLogin(params: SocialLoginParams): Promise<IToken> {
        const uri = `/social-auth/facebook/`;
        const r = await ApiClient.post(uri, params, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async connectFacebook(params: SocialLoginParams): Promise<IToken> {
        const uri = `/social-auth/facebook/connect/`;
        return ApiClient.post(uri, params);
    }

    async googleLogin(params: SocialLoginParams): Promise<IToken> {
        const uri = `/social-auth/google/`;
        const r = await ApiClient.post(uri, params, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async twitterLogin(params: SocialLoginParams): Promise<IToken> {
        const uri = `/social-auth/twitter/`;
        const r = await ApiClient.post(uri, params, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async linkedInLogin(params: SocialLoginParams): Promise<IToken> {
        const uri = `/social-auth/linkedin/`;
        const r = await ApiClient.post(uri, params, {}, false);
        await this.setAuthToken(r);
        return r;
    }
}
