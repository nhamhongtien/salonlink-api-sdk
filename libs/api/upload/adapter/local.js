"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../../../http/client");
const adapter_1 = require("./adapter");
const formDataHeaders = { 'Content-Type': 'multipart/form-data' };
class LocalUploadFile extends adapter_1.AbstractUploadAdapter {
    upload(params, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield client_1.ApiClient.post('/uploads/', params, {}, formDataHeaders);
            if (Array.isArray(res) && res.length > 0) {
                return res[0];
            }
            return { file_path: '' };
        });
    }
    uploadMultiFiles(params, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield client_1.ApiClient.post('/uploads/', params, {}, formDataHeaders);
        });
    }
    uploadImage(params, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            params.append('folder_name', 'photos');
            return yield this.upload(params, callback);
        });
    }
    uploadMultiImages(params, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            params.append('folder_name', 'photos');
            return yield this.uploadMultiFiles(params, callback);
        });
    }
}
exports.LocalUploadFile = LocalUploadFile;
