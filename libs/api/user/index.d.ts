import { IUser, IUserProfile } from './models';
export interface UserRegisterParams {
    username?: string;
    email: string;
    password1: string;
    password2: string;
    first_name?: string;
    last_name?: string;
    bio?: string;
    avatar?: string;
}
export interface UserProfileParams {
    first_name?: string;
    last_name?: string;
    phone: string;
    bio?: string;
    avatar?: string;
}
export interface UpdateEmailParams {
    email: string;
}
export declare class User {
    static register(params: UserRegisterParams): Promise<IUser>;
    static me(): Promise<IUser>;
    static profile(): Promise<IUserProfile>;
    static updateProfile(data: UserProfileParams): Promise<IUserProfile>;
    static existsUser(username: string): Promise<boolean>;
    static existsEmail(email: string): Promise<boolean>;
    static updateEmail(params: UpdateEmailParams): Promise<IUser>;
}
