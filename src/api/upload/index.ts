import { LocalUploadFile } from './adapter/local';

const UploadFile = new LocalUploadFile();

export { UploadFile };