import { ApiClient } from '../../http/client';
import { INotification, INotificationDevice } from './models';
import { YesNo, PlatForm } from '../../type';
import { IPagination } from '../../type';

export interface MarkAsReadParams {
    all: YesNo; // either all or ids is required
    ids: Array<string>;
}

export interface MarkAsArchivedParams extends MarkAsReadParams { }

export interface RegisterDeviceParam {
    name?: string;
    registration_id: string;
    device_id?: string;
    type?: PlatForm;
}

export class Notification {
    static async registerDevice(params: RegisterDeviceParam): Promise<INotificationDevice> {
        const uri = '/notifications/fcm/devices/';
        return ApiClient.post(uri, params);
    }

    static async unregisterDevice(registrationId: string): Promise<void> {
        const uri = `/notifications/fcm/devices/${registrationId}/`;
        return ApiClient.delete(uri);
    }

    static async list(q?: string, page: number = 1, limit: number = 20): Promise<IPagination<INotification>> {
        const params = { search: q, page, limit };
        return ApiClient.get('/notifications/', params);
    }

    static async maskAsRead(ids: Array<string>): Promise<void> {
        const uri = `/notifications/read/`;
        const params: MarkAsReadParams = {
            all: YesNo.NO,
            ids: ids
        };
        return ApiClient.post(uri, params);
    }

    static async maskAsReadAll(): Promise<void> {
        const uri = `/notifications/read/`;
        const params: MarkAsReadParams = {
            all: YesNo.YES,
            ids: []
        };
        return ApiClient.post(uri, params);
    }

    static async maskAsArchived(ids: Array<string>): Promise<void> {
        const uri = `/notifications/archive/`;
        const params: MarkAsArchivedParams = {
            all: YesNo.NO,
            ids: ids
        };
        return ApiClient.post(uri, params);
    }

    static async maskAsArchivedAll(): Promise<void> {
        const uri = `/notifications/archive/`;
        const params: MarkAsArchivedParams = {
            all: YesNo.YES,
            ids: []
        };
        return ApiClient.post(uri, params);
    }

    static async getBadge(): Promise<number> {
        const uri = `/notifications/badge/`;
        const r = await ApiClient.get(uri);
        return r.badge || 0;
    }
}
