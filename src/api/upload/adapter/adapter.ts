import { ApiClient } from "../../../http/client";
import {IPreSignedUrlPost, IUploadFile} from '../models';

export interface PreSignedUrlParam {
    file_name: string;
    content_type: string;
    folder: string;
    is_public: boolean;
}

export abstract class AbstractUploadAdapter {
    abstract async upload(params: any, callback?: any): Promise<IUploadFile>;

    async preSignedUrl(params: PreSignedUrlParam): Promise<IPreSignedUrlPost> {
        return await ApiClient.post('/uploads/pre-signed-url/', params);
    }
}