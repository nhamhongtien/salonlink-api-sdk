require('./config');
const { SocketIO, api } = require('../libs/index');

const socket = SocketIO.instance('http://localhost:4000');
const jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTc1MzAzMzI3LCJqdGkiOiI3NWRiZjUxMDlmYjY0ZmM5OGIxZTBiNmRmMmQxNmRhYSIsInVzZXJfaWQiOiIwZWIxZjUwYS0wYjljLTRlNDgtOTJiNS1hZmE1ZDhiMGZlYTcifQ.iSh6LiQAM98nzKYNAH7V4-K5hj4bRl45GAVqerzLVh4JSEXCCPlhM24lQPFwXX__NeM1pnN5NDo2xEXDjIIsmo41ckjbXYBJvh6gWmzbJk0HtZkK368X54pD4WU02KbJJgdXKD6t4Bzj3mrDzXcmMSe4DLjdBV9odsKW07c0dok';
socket.connect(jwt);
socket.addEventListener('joined', (evt) => {
    console.log('joined event: ', evt);
});
socket.addEventListener('joined', (evt) => {
    console.log('joined event 2: ', evt);
});
socket.addEventListener('error', (evt) => {
    console.log('error event: ', evt);
});

socket.addEventListener('hello', (evt) => {
    console.log('hello event: ', evt);
});