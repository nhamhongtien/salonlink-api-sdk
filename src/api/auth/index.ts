import { ApiClient } from '../..';
import { JwtAuth } from './adapter/jwt';

const Auth = new JwtAuth();
ApiClient.setAuthenticator(Auth);

export { Auth };