export interface ISession {
    set(key: string, value: string): Promise<void>;
    get(key: string): Promise<string>;
    remove(key: string): Promise<any>;
}
export interface ApiConfiguration {
    baseUrl: string;
    session: ISession;
    AUTH_SESSION_KEY: string;
    REFRESH_SESSION_KEY: string;
    appVersion?: string;
    deviceId?: string;
    userAgent: string;
    clientId?: string;
    socketUrl?: string;
}
