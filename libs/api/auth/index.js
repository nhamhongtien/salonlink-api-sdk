"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("../..");
const jwt_1 = require("./adapter/jwt");
const Auth = new jwt_1.JwtAuth();
exports.Auth = Auth;
__1.ApiClient.setAuthenticator(Auth);
