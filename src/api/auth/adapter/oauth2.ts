import { ApiClient, IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter } from './adapter';
import * as utils from '../../../utils';

export interface IOAuth2Token {
    access_token: string;
    refresh_token: string;
    token_type?: string;
    scope?: string;
    expires_in?: number;
}

export class Oauth2Auth extends AbstractAuthAdapter {

    async login(username: string, password: string): Promise<IOAuth2Token> {
        const data = { username, password };
        const r = await ApiClient.post('/o/token/', data, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async getAuthToken(): Promise<IAuthToken | null> {
        const config = ApiClient.getApiConfig();
        const json: string = await config.session.get(config.AUTH_SESSION_KEY);
        const token = utils.tryParseJson(json) as IOAuth2Token;
        if (!token || !token.access_token) {
            return null;
        }
        if (!token || !token.access_token) {
            return null;
        }
        return token;
    }

    async refreshToken(): Promise<void> {
        const token = await this.getAuthToken();
        if (!token) {
            return;
        }
        const data = { refresh_token: token.refresh_token };
        const r = await ApiClient.post('/o/token/', data, {}, false);
        await this.setAuthToken(r);
        return r;
    }
}
