require('./config');
const { api, ApiClient } = require('../libs/index');

(async () => {
    const username = 'quynh.le@hdwebsoft.com';
    const password = '123456@aA';
    try {
        const login = await api.Auth.login(username, password);
        console.log('LOGIN: ', login);
        console.log('Authentication Token: ', await ApiClient.authenticator.getAuthToken());
        console.log('Authentication Header: ', await ApiClient.authenticator.getAuthHeader());

        const user = await api.User.me();
        console.log('USER: ', user);
    } catch (error) {
        console.log('error: ', error);
    }
})();
