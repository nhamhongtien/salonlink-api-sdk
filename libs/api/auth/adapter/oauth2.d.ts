import { IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter } from './adapter';
export interface IOAuth2Token {
    access_token: string;
    refresh_token: string;
    token_type?: string;
    scope?: string;
    expires_in?: number;
}
export declare class Oauth2Auth extends AbstractAuthAdapter {
    login(username: string, password: string): Promise<IOAuth2Token>;
    getAuthToken(): Promise<IAuthToken | null>;
    refreshToken(): Promise<void>;
}
