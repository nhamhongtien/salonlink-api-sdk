import * as io from 'socket.io-client';

export class SocketEvent {
    constructor(
        public name: string,
        public payload?: { [key: string]: any }
    ) { }
}

export interface ISocketEventListener {
    /**
     * @param event SocketEvent
     */
    (event: SocketEvent): Promise<void>;
}

export class SocketIO {

    private static _instance?: SocketIO;
    private socket: any = null;
    private events: { [eventName: string]: Array<ISocketEventListener> } = {};

    private constructor(private url: string) {
    }

    static instance(url: string): SocketIO {
        if (!SocketIO._instance) {
            SocketIO._instance = new SocketIO(url);
        }
        return SocketIO._instance;
    }

    connect(jwt: string): void {
        if (this.isConnected()) {
            this.close();
        }
        const socket = io(this.url, {
            transports: ["websocket"]
        });
        this.socket = socket;

        socket.on("connected", (data: any) => {
            socket.emit("join", jwt);
            for (let eventName in this.events) {
                if (!this.events[eventName]) {
                    continue;
                }
                socket.on(eventName, (data: any) => this.handleEvent(eventName, data));
            }
        });

        socket.on("error", (e: Error) => { });
    }

    get id(): string | null {
        if (!this.connected) {
            return null;
        }
        return this.socket.id;
    }

    get connected(): boolean {
        if (this.socket && this.socket.connected) {
            return true;
        }
        return false;
    }

    isConnected(): boolean {
        return this.connected;
    }

    close(): void {
        if (this.socket) {
            this.socket.close();
            this.socket = null;
        }
    }

    disconnect(): void {
        this.close();
    }

    on(eventName: string, listener: ISocketEventListener): void {
        if (!this.socket) {
            return;
        }
        this.socket.on(eventName, (payload: any) => {
            const evt = new SocketEvent(eventName, payload);
            listener(evt);
        });
    }

    emit(eventName: string, payload: any): this {
        if (!this.connected) {
            return this;
        }
        this.socket.emit(eventName, payload);
        return this;
    }

    /**
     * Add an event handler for given eventName
     * 
     * @param {string} eventName 
     * @param {function|object} listener a function or an instance of ISocketEventListener
     */
    addEventListener(eventName: string, listener: ISocketEventListener): this {
        if (!eventName) {
            return this;
        }
        if (!(eventName in this.events)) {
            this.events[eventName] = [];
        }
        this.events[eventName].push(listener);
        return this;
    }

    private async handleEvent(eventName: string, payload: any): Promise<void> {
        const listeners = this.events[eventName];
        if (!Array.isArray(listeners) || listeners.length === 0) {
            return;
        }
        const evt = new SocketEvent(eventName, payload);
        for (let listener of listeners) {
            if (typeof listener === 'function') {
                return listener(evt);
            }
        }
    }
}
