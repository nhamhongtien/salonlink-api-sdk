"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../../../http/client");
const adapter_1 = require("./adapter");
const utils = require("../../../utils");
class JwtAuth extends adapter_1.AbstractAuthAdapter {
    login(username, password) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { email: username, password };
            const r = yield client_1.ApiClient.post('/token/', data, {}, false);
            yield this.setAuthToken(r);
            return r;
        });
    }
    getAuthToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const config = client_1.ApiClient.getApiConfig();
            const jwtJson = yield config.session.get(config.AUTH_SESSION_KEY);
            const jtwToken = utils.tryParseJson(jwtJson);
            if (!jtwToken || !jtwToken.access) {
                return null;
            }
            return {
                token_type: 'Bearer',
                access_token: jtwToken.access,
                refresh_token: jtwToken.refresh
            };
        });
    }
    refreshToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield this.getAuthToken();
            if (!token) {
                return;
            }
            const refresh = token.refresh_token;
            const data = { refresh };
            const r = yield client_1.ApiClient.post('/token/refresh/', data, {}, false);
            yield this.setAuthToken(r);
        });
    }
}
exports.JwtAuth = JwtAuth;
