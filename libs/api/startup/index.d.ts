export interface IStartupVersion {
    minimal_version: string;
    current_version: string;
    title: string;
    message?: string;
    icon?: string;
    background?: string;
    platform: string;
    app_store_url: string;
}
export interface IStartup {
    update: IStartupVersion;
}
export declare class Startup {
    static get(): Promise<IStartup>;
}
