import { ApiClient } from '../../http/client';
import { IUser, IUserProfile } from './models';

export interface UserRegisterParams {
    username?: string;
    email: string;
    password1: string;
    password2: string;
    first_name?: string;
    last_name?: string;
    bio?: string;
    avatar?: string;
}

export interface UserProfileParams {
    first_name?: string;
    last_name?: string;
    phone: string;
    bio?: string;
    avatar?: string;
}

export interface UpdateEmailParams {
    email: string;
}


export class User {
    static async register(params: UserRegisterParams): Promise<IUser> {
        return ApiClient.post('/auth/registration/', params, {}, false);
    }

    static async me(): Promise<IUser> {
        return ApiClient.get('/users/me/');
    }

    static async profile(): Promise<IUserProfile> {
        return ApiClient.get('/profile/');
    }

    static async updateProfile(data: UserProfileParams): Promise<IUserProfile> {
        const uri = '/users/profile/';
        return ApiClient.patch(uri, data, {});
    }

    static async existsUser(username: string): Promise<boolean> {
        const uri = `/users/exists/`;
        const r = await ApiClient.get(uri, { username });
        return r.exists;
    }

    static async existsEmail(email: string): Promise<boolean> {
        const uri = `/users/exists/`;
        const r = await ApiClient.get(uri, { email });
        return r.exists;
    }

    static async updateEmail(params: UpdateEmailParams): Promise<IUser> {
        const uri = `/users/email/change/`;
        return ApiClient.patch(uri, params);
    }
}
