import axios from 'axios';
import { ApiConfiguration } from "./config";

class ApiError extends Error {
    constructor(public message: string, public code: string) {
        super(message);
    }
}

export interface IAuthToken {
    access_token: string;
    refresh_token?: string;
    token_type?: string;
    scope?: string;
    expires_in?: number;
}

export interface IAuthenticator {
    getAuthToken(): Promise<IAuthToken | null>;
    getAuthHeader(): Promise<string>;
}

export class ApiClient {
    static apiConfig: ApiConfiguration;
    static authenticator: IAuthenticator;

    public static setApiConfig(apiConfig: ApiConfiguration): void {
        this.apiConfig = apiConfig;
    }

    public static getApiConfig(): ApiConfiguration {
        return this.apiConfig;
    }

    public static setAuthenticator(authenticator: IAuthenticator) {
        this.authenticator = authenticator;
        return this;
    }

    public static async getAuthToken(): Promise<IAuthToken | null> {
        if (!this.authenticator) {
            return null;
        }
        return this.authenticator.getAuthToken();
    }

    private static success(response: any): any {
        return response.data;
    }

    private static error(e: any): any {
        if (e.response && e.response.data) {
            const errorCode = e.response.data.code || 'unknown';
            const message = e.response.data.message || e.message;
            throw new ApiError(message, errorCode);
        }
        return this.handleUnknownError(e);
    }

    private static handleUnknownError(e: Error) {
        throw new ApiError(e.message, 'unknown');
    }

    private static uri(uri: string): string {
        return `${this.apiConfig.baseUrl}${uri}`;
    }

    private static async configs(params: any = {}, headers: any = {}, isAuthenticated: boolean = true): Promise<any> {
        const authHeader = await this.authenticator.getAuthHeader();
        const baseUrl = this.apiConfig.baseUrl;
        if (authHeader !== '' && isAuthenticated) {
            headers = {
                ...headers,
                Authorization: authHeader
            };
        }
        headers = {
            ...headers,
            'App-Version': this.apiConfig.appVersion,
            'Device-Id': this.apiConfig.deviceId,
            'User-Agent': this.apiConfig.userAgent,
            'Client-Id': this.apiConfig.clientId
        };
        return { baseUrl, headers, params, withCredentials: false, crossdomain: true };
    }

    static async get(uri: string, params: any = {}, isAuthenticated: boolean = true): Promise<any> {
        try {
            const config = await this.configs(params, isAuthenticated);
            const r = await axios.get(this.uri(uri), config);
            return this.success(r);
        } catch (e) {
            return this.error(e);
        }
    }

    static async post(uri: string, data: any = {}, params: any = {}, headers: any = {}, isAuthenticated: boolean = true): Promise<any> {
        try {
            const config = await this.configs(params, headers, isAuthenticated);
            const r = await axios.post(this.uri(uri), data, config);
            return this.success(r);
        } catch (e) {
            this.error(e);
        }
    }

    static async patch(uri: string, data: any = {}, params: any = {}, headers: any = {}, isAuthenticated: boolean = true): Promise<any> {
        try {
            const config = await this.configs(params, headers, isAuthenticated);
            const r = await axios.patch(this.uri(uri), data, config);
            return this.success(r);
        } catch (e) {
            this.error(e);
        }
    }

    static async put(uri: string, data: any = {}, params: any = {}, isAuthenticated: boolean = true): Promise<any> {
        try {
            const config = await this.configs(params, isAuthenticated);
            const r = await axios.put(this.uri(uri), data, config);
            return this.success(r);
        } catch (e) {
            this.error(e);
        }
    }

    static async delete(uri: string, params: any = {}, isAuthenticated: boolean = true): Promise<any> {
        try {
            const config = await this.configs(params, isAuthenticated);
            const r = await axios.delete(this.uri(uri), config);
            return this.success(r);
        } catch (e) {
            this.error(e);
        }
    }

    static async upload(uri: string, params: any = {}, callback: any): Promise<any> {
        try {
            const r = await axios.post(uri, params,
            {
                onUploadProgress: function(progressEvent: any) {
                    var loaded = progressEvent.loaded;
                    var total = progressEvent.total;
                    var percent = Math.round((loaded / total) * 100);
                    callback(percent);
                }.bind(this)
            });
            return this.success(r);
        } catch (e) {
            this.error(e);
        }
    }

}