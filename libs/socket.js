"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const io = require("socket.io-client");
class SocketEvent {
    constructor(name, payload) {
        this.name = name;
        this.payload = payload;
    }
}
exports.SocketEvent = SocketEvent;
class SocketIO {
    constructor(url) {
        this.url = url;
        this.socket = null;
        this.events = {};
    }
    static instance(url) {
        if (!SocketIO._instance) {
            SocketIO._instance = new SocketIO(url);
        }
        return SocketIO._instance;
    }
    connect(jwt) {
        if (this.isConnected()) {
            this.close();
        }
        const socket = io(this.url, {
            transports: ["websocket"]
        });
        this.socket = socket;
        socket.on("connected", (data) => {
            socket.emit("join", jwt);
            for (let eventName in this.events) {
                if (!this.events[eventName]) {
                    continue;
                }
                socket.on(eventName, (data) => this.handleEvent(eventName, data));
            }
        });
        socket.on("error", (e) => { });
    }
    get id() {
        if (!this.connected) {
            return null;
        }
        return this.socket.id;
    }
    get connected() {
        if (this.socket && this.socket.connected) {
            return true;
        }
        return false;
    }
    isConnected() {
        return this.connected;
    }
    close() {
        if (this.socket) {
            this.socket.close();
            this.socket = null;
        }
    }
    disconnect() {
        this.close();
    }
    on(eventName, listener) {
        if (!this.socket) {
            return;
        }
        this.socket.on(eventName, (payload) => {
            const evt = new SocketEvent(eventName, payload);
            listener(evt);
        });
    }
    emit(eventName, payload) {
        if (!this.connected) {
            return this;
        }
        this.socket.emit(eventName, payload);
        return this;
    }
    /**
     * Add an event handler for given eventName
     *
     * @param {string} eventName
     * @param {function|object} listener a function or an instance of ISocketEventListener
     */
    addEventListener(eventName, listener) {
        if (!eventName) {
            return this;
        }
        if (!(eventName in this.events)) {
            this.events[eventName] = [];
        }
        this.events[eventName].push(listener);
        return this;
    }
    handleEvent(eventName, payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const listeners = this.events[eventName];
            if (!Array.isArray(listeners) || listeners.length === 0) {
                return;
            }
            const evt = new SocketEvent(eventName, payload);
            for (let listener of listeners) {
                if (typeof listener === 'function') {
                    return listener(evt);
                }
            }
        });
    }
}
exports.SocketIO = SocketIO;
