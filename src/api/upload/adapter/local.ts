import { ApiClient } from "../../../http/client";
import { AbstractUploadAdapter } from './adapter';
import { IUploadFile } from '../models';

const formDataHeaders = { 'Content-Type': 'multipart/form-data' };

export class LocalUploadFile extends AbstractUploadAdapter {

    async upload(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile> {
        const res: IUploadFile[] = await ApiClient.post('/uploads/', params, {}, formDataHeaders);
        if (Array.isArray(res) && res.length > 0) {
            return res[0];
        }
        return { file_path: '' };
    }

    async uploadMultiFiles(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile[]> {
        return await ApiClient.post('/uploads/', params, {}, formDataHeaders);
    }

    async uploadImage(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile> {
        params.append('folder_name', 'photos');
        return await this.upload(params, callback);
    }

    async uploadMultiImages(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile[]> {
        params.append('folder_name', 'photos');
        return await this.uploadMultiFiles(params, callback);
    }
}
