import { ApiConfiguration } from "./config";
export interface IAuthToken {
    access_token: string;
    refresh_token?: string;
    token_type?: string;
    scope?: string;
    expires_in?: number;
}
export interface IAuthenticator {
    getAuthToken(): Promise<IAuthToken | null>;
    getAuthHeader(): Promise<string>;
}
export declare class ApiClient {
    static apiConfig: ApiConfiguration;
    static authenticator: IAuthenticator;
    static setApiConfig(apiConfig: ApiConfiguration): void;
    static getApiConfig(): ApiConfiguration;
    static setAuthenticator(authenticator: IAuthenticator): typeof ApiClient;
    static getAuthToken(): Promise<IAuthToken | null>;
    private static success;
    private static error;
    private static handleUnknownError;
    private static uri;
    private static configs;
    static get(uri: string, params?: any, isAuthenticated?: boolean): Promise<any>;
    static post(uri: string, data?: any, params?: any, headers?: any, isAuthenticated?: boolean): Promise<any>;
    static patch(uri: string, data?: any, params?: any, headers?: any, isAuthenticated?: boolean): Promise<any>;
    static put(uri: string, data?: any, params?: any, isAuthenticated?: boolean): Promise<any>;
    static delete(uri: string, params?: any, isAuthenticated?: boolean): Promise<any>;
    static upload(uri: string, params: any, callback: any): Promise<any>;
}
