import { ApiClient, IAuthenticator, IAuthToken } from "../../../http/client";

export interface SocialLoginParams {
    access_token: string;
    token_secret?: string;
    code?: string;
    provider?: string;
}

export interface IAuthorizationHeaders {
    Authorization: string;
}

export interface UserRegisterParams {
    username?: string;
    email: string;
    password1: string;
    password2: string;
    first_name?: string;
    last_name?: string;
    bio?: string;
}


export interface IAuthAdapter extends IAuthenticator {
    login(username: string, password: string): Promise<any>;
    logout(): void;
    register(params: UserRegisterParams): Promise<any>;
    refreshToken(): void;
    setAuthToken(data: any): Promise<void>;
    isAuthenticated(): Promise<boolean>;
    getAuthorizationHeaders(): Promise<IAuthorizationHeaders>;
}

export interface ISocalAuth {
    facebookLogin(params: SocialLoginParams): Promise<any>;
    connectFacebook(params: SocialLoginParams): Promise<any>;
    googleLogin(params: SocialLoginParams): Promise<any>;
    twitterLogin(params: SocialLoginParams): Promise<any>;
    linkedInLogin(params: SocialLoginParams): Promise<any>;
}

export interface IChangePassword {
    changePassword(oldPassword: string, newPassword: string): Promise<any>;
    resetPassword(email: string): Promise<any>;
    resetPasswordConfirm(newPassword: string, otp: string, email: string): Promise<any>;
}

export abstract class AbstractAuthAdapter implements IAuthAdapter, IChangePassword {

    abstract async login(username: string, password: string): Promise<any>;
    abstract async getAuthToken(): Promise<IAuthToken | null>;
    abstract async refreshToken(): Promise<void>;

    async getAuthHeader(): Promise<string> {
        const authToken = await this.getAuthToken();
        if (!authToken || !authToken.access_token) {
            return '';
        }
        if (authToken.token_type) {
            return `${authToken.token_type} ${authToken.access_token}`;
        }
        return authToken.access_token;
    }

    async logout(): Promise<any> {
        const config = ApiClient.getApiConfig();
        const sessionStorage = config.session;
        await sessionStorage.remove(config.AUTH_SESSION_KEY);
    }

    async setAuthToken(data: any): Promise<void> {
        const config = ApiClient.getApiConfig();
        const sessionStorage = config.session;
        await sessionStorage.set(config.AUTH_SESSION_KEY, JSON.stringify(data));
    }

    async isAuthenticated(): Promise<boolean> {
        const token = await this.getAuthToken();
        if (token) {
            return true;
        }
        return false;
    }

    async getAuthorizationHeaders(): Promise<IAuthorizationHeaders> {
        const authHeader = await this.getAuthHeader();
        const headers: IAuthorizationHeaders = {
            Authorization: authHeader
        };
        return headers;
    }

    async changePassword(oldPassword: string, newPassword: string): Promise<any> {
        const uri = `/auth/password/change/`;
        const params = {
            old_password: oldPassword,
            new_password1: newPassword,
            new_password2: newPassword
        };
        return ApiClient.post(uri, params);
    }

    async resetPassword(email: string): Promise<any> {
        const uri = `/auth/password/reset/`;
        const params = {
            email: email
        };
        return ApiClient.post(uri, params);
    }

    async resetPasswordConfirm(newPassword: string, otp: string, email: string): Promise<any> {
        const uri = `/auth/password/reset/confirm/`;
        const params = {
            new_password1: newPassword,
            new_password2: newPassword,
            otp: otp,
            email: email
        };
        return ApiClient.post(uri, params);
    }

    async register(params: UserRegisterParams): Promise<any> {
        const r = await ApiClient.post('/auth/registration/', params, {}, false);
        await this.setAuthToken(r);
        return r;
    }
}
