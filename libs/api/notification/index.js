"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../../http/client");
const type_1 = require("../../type");
class Notification {
    static registerDevice(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = '/notifications/fcm/devices/';
            return client_1.ApiClient.post(uri, params);
        });
    }
    static unregisterDevice(registrationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/fcm/devices/${registrationId}/`;
            return client_1.ApiClient.delete(uri);
        });
    }
    static list(q, page = 1, limit = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = { search: q, page, limit };
            return client_1.ApiClient.get('/notifications/', params);
        });
    }
    static maskAsRead(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/read/`;
            const params = {
                all: type_1.YesNo.NO,
                ids: ids
            };
            return client_1.ApiClient.post(uri, params);
        });
    }
    static maskAsReadAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/read/`;
            const params = {
                all: type_1.YesNo.YES,
                ids: []
            };
            return client_1.ApiClient.post(uri, params);
        });
    }
    static maskAsArchived(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/archive/`;
            const params = {
                all: type_1.YesNo.NO,
                ids: ids
            };
            return client_1.ApiClient.post(uri, params);
        });
    }
    static maskAsArchivedAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/archive/`;
            const params = {
                all: type_1.YesNo.YES,
                ids: []
            };
            return client_1.ApiClient.post(uri, params);
        });
    }
    static getBadge() {
        return __awaiter(this, void 0, void 0, function* () {
            const uri = `/notifications/badge/`;
            const r = yield client_1.ApiClient.get(uri);
            return r.badge || 0;
        });
    }
}
exports.Notification = Notification;
