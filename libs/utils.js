"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tryParseJson = (jsonString) => {
    try {
        return JSON.parse(jsonString);
    }
    catch (error) {
        return jsonString;
    }
};
