import { IAuthenticator, IAuthToken } from "../../../http/client";
export interface SocialLoginParams {
    access_token: string;
    token_secret?: string;
    code?: string;
    provider?: string;
}
export interface IAuthorizationHeaders {
    Authorization: string;
}
export interface UserRegisterParams {
    username?: string;
    email: string;
    password1: string;
    password2: string;
    first_name?: string;
    last_name?: string;
    bio?: string;
}
export interface IAuthAdapter extends IAuthenticator {
    login(username: string, password: string): Promise<any>;
    logout(): void;
    register(params: UserRegisterParams): Promise<any>;
    refreshToken(): void;
    setAuthToken(data: any): Promise<void>;
    isAuthenticated(): Promise<boolean>;
    getAuthorizationHeaders(): Promise<IAuthorizationHeaders>;
}
export interface ISocalAuth {
    facebookLogin(params: SocialLoginParams): Promise<any>;
    connectFacebook(params: SocialLoginParams): Promise<any>;
    googleLogin(params: SocialLoginParams): Promise<any>;
    twitterLogin(params: SocialLoginParams): Promise<any>;
    linkedInLogin(params: SocialLoginParams): Promise<any>;
}
export interface IChangePassword {
    changePassword(oldPassword: string, newPassword: string): Promise<any>;
    resetPassword(email: string): Promise<any>;
    resetPasswordConfirm(newPassword: string, otp: string, email: string): Promise<any>;
}
export declare abstract class AbstractAuthAdapter implements IAuthAdapter, IChangePassword {
    abstract login(username: string, password: string): Promise<any>;
    abstract getAuthToken(): Promise<IAuthToken | null>;
    abstract refreshToken(): Promise<void>;
    getAuthHeader(): Promise<string>;
    logout(): Promise<any>;
    setAuthToken(data: any): Promise<void>;
    isAuthenticated(): Promise<boolean>;
    getAuthorizationHeaders(): Promise<IAuthorizationHeaders>;
    changePassword(oldPassword: string, newPassword: string): Promise<any>;
    resetPassword(email: string): Promise<any>;
    resetPasswordConfirm(newPassword: string, otp: string, email: string): Promise<any>;
    register(params: UserRegisterParams): Promise<any>;
}
