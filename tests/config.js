const { ApiClient } = require('../libs/index');

const config = {
    baseUrl: 'http://salonlinks-api.dev.goldfishcode.com/v1',
    AUTH_SESSION_KEY: 'oauth_token',
    appVersion: '1.0.0',
    deviceId: '123456789',
    userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/72.0.3626.101 Mobile/15E148 Safari/605.1',
    clientId: 'tenant1',
    cache: {},
    session: {
        set: async (key, value) => { 
            config.cache[key] = value;
        },

        get: async (key) => {
            return config.cache[key];
        },
        remove: async (key) => {
            delete config.cache[key];
        },
    },
    socketUrl: 'http://localhost:4000'
};

ApiClient.setApiConfig(config);