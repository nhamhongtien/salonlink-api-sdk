const { api, ApiClient } = require('./libs/index');
const MessageType = require('./libs/type');

const config = {
    baseUrl: 'https://salonlinks-api.dev.goldfishcode.com/v1',
    AUTH_SESSION_KEY: 'oauth_token',
    REFRESH_SESSION_KEY: 'refresh_token',
    appVersion: '1.0.0',
    deviceId: '123456789',
    userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/72.0.3626.101 Mobile/15E148 Safari/605.1',
    clientId: 'tenant1',
    session: {
        set: async (key, value) => { },
        get: async (key) => {
            if (key === 'oauth_token')
                return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTczNDY5OTk3LCJqdGkiOiI2Y2I3OGQ3MDg1YzU0N2JmYWVhZWE2YTlmODYyZjM1OCIsInVzZXJfaWQiOiI0ZmEyOGVmNC1lOTI2LTQwOGYtODgzZC1iN2VhZmQ2ZWFkODcifQ.TIKBRv96JrD4gZ9nHcSodNMaC5N7HeIE5RmetttbaCA';
            else if (key === 'refresh_token')
                return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU3MzU1NTQ4MSwianRpIjoiMzg5ODg5OGM0YjViNGM3Njg4MGJhZmI3YWMyNTNhMDkiLCJ1c2VyX2lkIjoiNGZhMjhlZjQtZTkyNi00MDhmLTg4M2QtYjdlYWZkNmVhZDg3In0.AoDkaSl1sxUuyRRgxDd6xyDYCGIgVfNmGOwbebrz93c';
        },
        remove: async (key, value) => { },
    }
};

ApiClient.setApiConfig(config);

(async () => {
    const username = 'quynh.le@hdwebsoft.com';
    const password = '123456@aA';
    const newPassword = '12345678@';
    const otp = '890498';
    try {
        // const login = await api.Auth.login(username, password);
        // console.log('LOGIN: ', login);

        // const login = await api.Auth.auth(username, password);
        // console.log('LOGIN: ', login);

        // const r = await api.User.me();
        // console.log('LOGIN R: ', r);

        // const r = await api.Contact.list();
        // console.log('Contacts: ', r);

        // const device = {
        //     name: 'test device',
        //     registration_id: 'xx893840284024803434832424',
        //     type: 'ios'
        // };
        // const r = await api.Notification.registerDevice(device);
        // console.log('device: ', r);

        // const changePassword = await api.Auth.changePassword(password, newPassword);
        // console.log('CHANGE PASSWORD: ', changePassword);

        // const resetPassword = await api.Auth.resetPassword(username);
        // console.log('RESET PASSWORD: ', resetPassword);
        // const resetPasswordConfirm = await api.Auth.resetPasswordConfirm(password, otp, username);
        // console.log('RESET PASSWORD CONFIRM: ', resetPasswordConfirm);

        // const r = await api.Shop.listHomeCategories();
        // console.log('Shop home categories: ', r);

        // const r = await api.Shop.listProductsByCategory('1227');
        // console.log('Shop product by category: ', r);

        // const r = await api.Shop.listProductComments('25731');
        // console.log('Shop product comments: ', r);

        // const params = {
        //     productId: '25731',
        //     message: 'Testing lorem isum dplo',
        //     rating: 5
        // }
        // const r = await api.Shop.editProductComment('234', params);
        // console.log('Shop edit product comment:', r);

        // const r = await api.Shop.listOrders();
        // console.log('Shop orders: ', r);

        // Create 1 time card token here https://stripe.com/docs/api/tokens/create_card 
        // with stripe_public_key from getAppConfig() 
        // const orderParams = {
        //     "card_token": "tok_1FZEKxJGvqfc78YnSd7n9UoO",
        //     "currency": "USD",
        //     "total": "32.76",
        //     "subtotal": "32.76",
        //     "shipping_method_name": "Flat Rate",
        //     "shipping_amount": "10.00",
        //     "tax_rate": "10%",
        //     "Items": [
        //       {
        //         "productId": "25722",
        //         "qty": 1,
        //         "price": "32.76",
        //         "tax": true,
        //         "tax_amount": "3.28"
        //       }
        //     ],
        //     "billing": [
        //       {
        //         "first_name": "Lam",
        //         "last_name": "Test",
        //         "email": "lam.test5@gmail.com",
        //         "phone": "323-962-1052",
        //         "address1": "3385  Felosa Drive",
        //         "address2": "3385  Felosa Drive",
        //         "city": "Hollywood",
        //         "state": "CA",
        //         "postal": "90028",
        //         "country": "USA"
        //       }
        //     ],
        //     "shipping": [
        //       {
        //         "first_name": "Lam",
        //         "last_name": "Test",
        //         "email": "lam.test5@gmail.com",
        //         "phone": "323-962-1052",
        //         "address1": "3385  Felosa Drive",
        //         "address2": "3385  Felosa Drive",
        //         "city": "Hollywood",
        //         "state": "CA",
        //         "postal": "90028",
        //         "country": "USA"
        //       }
        //     ]
        //   }
        // const r = await api.Shop.createOrders(orderParams);
        // console.log('Shop created order: ', r);

        // const r = await api.Shop.getColors();
        // console.log('Shop colors: ', r);

        // const searchParams = {
        //     name: 'sim',
        //     sizes: ['s', 'm'],
        //     colors: ['blue'],
        //     prices: [0, 100]
        // }
        // const r = await api.Shop.searchProducts(searchParams);
        // console.log('Shop search products: ', r);

        // const r = await api.Shop.getShopConfig();
        // console.log('Shop config: ', r);

        // const lovePrams = {
        //     productId: '25744',
        //     love: '0',
        // }
        // const r = await api.Shop.loveProduct(lovePrams);

        // const params = {
        //     productId: '25731',
        //     message: 'Testing lorem isum dplo akgka',
        //     rating: 4
        // }
        // const r = await api.Shop.postProductComment(params);
        // console.log('Shop add product comment:', r);

        // const r = await api.Shop.getBrands(name='Sound');
        // console.log('Shop brands:', r);

        // const channels = await api.Chat.listChannels();
        // console.log('CHANNELS:', channels.results);

        // const messages = await api.Chat.listMessages('209daa4a-5b35-45cf-8a0e-6d64f26fd9b7');
        // console.log('MESSAGES: ', messages.results);

        // const message = {
        //     channel: '209daa4a-5b35-45cf-8a0e-6d64f26fd9b7',
        //     content: 'Hello How are you? What is going on today?'
        // };
        // const sendMessage = await api.Chat.sendMessage(message);
        // console.log('MESSAGE: ', sendMessage);

        // const messageDetail = await api.Chat.getMessageDetail('37b7f87a-f812-4618-b4d9-a09126016bee');
        // console.log('MESSAGE: ', messageDetail);

        // const channel = {
        //     'name': 'Chit chat chit chit chat 6',
        //     'participant_ids': ['6829bc5c-d30c-4413-8dfa-c408546e42a8']
        // }
        // const createChannel = await api.Chat.createChannel(channel);
        // console.log('CHANNEL: ', createChannel);

        // const formData = new FormData();
        // formData.append("name", "Chit chat chit chit chat 10");
        // const updateChannel = await api.Chat.updateChannel("3bd42563-68f7-4360-86bb-79e7ecb772e8", formData);
        // console.log('UPDATE CHANNEL: ' , updateChannel);

        // const readMessage = await api.Chat.readMessage('37b7f87a-f812-4618-b4d9-a09126016bee');
        // console.log('READ MESSAGE: ', readMessage);

        // const formData = new FormData();
        // formData.append('avatar', image_file);
        // const changeAvatar = await api.Chat.changeChannelAvatar('209daa4a-5b35-45cf-8a0e-6d64f26fd9b7');
        // console.log('CHANGE AVATAR: ', changeAvatar);

        // const formData = new FormData();
        // formData.append('files', files);
        // formData.append('channel', '209daa4a-5b35-45cf-8a0e-6d64f26fd9b7');
        // formData.append('type', MessageType.IMAGE)
        // const sendFiles = await api.Chat.sendFiles();
        // console.log('SEND FILES: ', sendFiles);
    } catch (error) {
        console.log('error: ', error);
    }
})();
