import { INotification, INotificationDevice } from './models';
import { YesNo, PlatForm } from '../../type';
import { IPagination } from '../../type';
export interface MarkAsReadParams {
    all: YesNo;
    ids: Array<string>;
}
export interface MarkAsArchivedParams extends MarkAsReadParams {
}
export interface RegisterDeviceParam {
    name?: string;
    registration_id: string;
    device_id?: string;
    type?: PlatForm;
}
export declare class Notification {
    static registerDevice(params: RegisterDeviceParam): Promise<INotificationDevice>;
    static unregisterDevice(registrationId: string): Promise<void>;
    static list(q?: string, page?: number, limit?: number): Promise<IPagination<INotification>>;
    static maskAsRead(ids: Array<string>): Promise<void>;
    static maskAsReadAll(): Promise<void>;
    static maskAsArchived(ids: Array<string>): Promise<void>;
    static maskAsArchivedAll(): Promise<void>;
    static getBadge(): Promise<number>;
}
