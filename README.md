# Salonlinks API Typescript SDK

Salonlinks API SDK in Typescript, it is used for Javascript app or NodeJS app.

## Installation

```bash
npm i -S git+ssh://git@gitlab.com:goldfish-projects/salonlinks-api-sdk.git
```

## Usage

### Session Storage

An implementation of `ISession` interface to store session data.

```javascript
class SessionStorage {
    // store key/value pair
    async set(key: string, value: string): Promise<void> {
        localStorage.setItem(key, value);
    }

    // get value of
    async get(key: string): Promise<string> {
        const value = localStorage.setItem(key);
        return (value || '')
    }

    // delete key
    async remove(key: string): Promise<void> {
        localStorage.removeItem(key);
    }
}

```

### Configuration

```javascript
import { api, ApiClient, SocketIO } from '@goldfishcode/salonlinks-api-sdk';

const sessionStorage = new SessionStorage();
const config = {
    baseUrl: 'http://salonlinks-api.dev.goldfishcode.com/v1',
    AUTH_SESSION_KEY: 'AUTH_SESSION_KEY',
    session: sessionStorage,
    socketUrl: 'http://salonlinks-ws.dev.goldfishcode.com'
};
ApiClient.setApiConfig(config);
```

### APIs

```javascript

// Login
const username = 'your username';
const password = 'your password';
const token = await api.Auth.login(username, password);

// Refresh Token, this API should be called when App is resume
if(api.Auth.getAuthToken()) {
    await api.Auth.refreshToken();
}

// User Profile
const user = await api.User.me();

// Register user
const registerParams = {
    "email": "test@test.com",
    "password1": "123456@aA",
    "password2": "123456@aA",
    "first_name": "Test",
    "last_name": "User"
};
const user = await api.Auth.register(registerParams);

```

### Socket

```javascript
// Initiate Web Socket connection
const socket = SocketIO.instance(config.socketUrl);
const token = ApiClient.getAuthToken();
if(token){
    socket.connect(token.access_token);
}
// Listen event
socket.on('joined', (data) => console.log(data));

// Emit event
socket.emit('eventName', 'Payload');

// To listen Socket Event of Chat module, we can simply using built-in listener like this:
const listener = api.Chat.SocketListener.instance();
listener.onMessage(msg => console.log('New message: ', msg));
listener.onChannelCreated(channel => console.log(channel));
```

## Development

```bash
npm install
npm run build
```
