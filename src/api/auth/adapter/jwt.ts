import { ApiClient, IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter } from './adapter';
import * as utils from '../../../utils';

export interface IJwtToken {
    refresh: string;
    access: string;
}

export class JwtAuth extends AbstractAuthAdapter {

    async login(username: string, password: string): Promise<IJwtToken> {
        const data = { email: username, password };
        const r: IJwtToken = await ApiClient.post('/token/', data, {}, false);
        await this.setAuthToken(r);
        return r;
    }

    async getAuthToken(): Promise<IAuthToken | null> {
        const config = ApiClient.getApiConfig();
        const jwtJson: string = await config.session.get(config.AUTH_SESSION_KEY);
        const jtwToken = utils.tryParseJson(jwtJson) as IJwtToken;
        if (!jtwToken || !jtwToken.access) {
            return null;
        }
        return {
            token_type: 'Bearer',
            access_token: jtwToken.access,
            refresh_token: jtwToken.refresh
        };
    }

    async refreshToken(): Promise<void> {
        const token = await this.getAuthToken();
        if (!token) {
            return;
        }
        const refresh = token.refresh_token;
        const data = { refresh };
        const r: IJwtToken = await ApiClient.post('/token/refresh/', data, {}, false);
        await this.setAuthToken(r);
    }
}
