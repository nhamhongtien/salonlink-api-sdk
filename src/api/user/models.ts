
export interface IUser {
    user_id: string;
    username: string;
    email: string;
    first_name: string;
    last_name: string;
    display_name?: string;
    bio?: string;
    phone?: string;
    avatar?: string;
}

export interface IUserProfile extends IUser {
}
