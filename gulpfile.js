'use strict';

const rimraf = require('gulp-rimraf');
const tslint = require('gulp-tslint');
const ts = require('gulp-typescript');
const outDir = 'libs';

const { src, dest, series } = require('gulp');

/**
 * Clean
 */
function clean() {
    return src(outDir, { read: false, allowEmpty: true })
        .pipe(rimraf());
}

/**
 * Compile typescript
 */

function compile() {
    const tsProject = ts.createProject('tsconfig.json');
    const tsResult = tsProject
        .src()
        .pipe(tsProject());
        //.pipe(ts({ declaration: true }));
    return tsResult.pipe(dest(outDir));
}

/**
 * Lint all custom TypeScript files.
 */
function lint() {
    return src('./src/**/*.ts')
        .pipe(tslint({
            formatter: 'prose'
        }))
        .pipe(tslint.report());
}

exports.clean = clean;
exports.tslint = lint;
exports.compile = compile;
exports.build = series(clean, lint, compile);