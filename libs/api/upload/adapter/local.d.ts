import { AbstractUploadAdapter } from './adapter';
import { IUploadFile } from '../models';
export declare class LocalUploadFile extends AbstractUploadAdapter {
    upload(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile>;
    uploadMultiFiles(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile[]>;
    uploadImage(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile>;
    uploadMultiImages(params: FormData, callback?: (progress: number) => void): Promise<IUploadFile[]>;
}
