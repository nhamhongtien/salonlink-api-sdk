import { IPreSignedUrlPost, IUploadFile } from '../models';
export interface PreSignedUrlParam {
    file_name: string;
    content_type: string;
    folder: string;
    is_public: boolean;
}
export declare abstract class AbstractUploadAdapter {
    abstract upload(params: any, callback?: any): Promise<IUploadFile>;
    preSignedUrl(params: PreSignedUrlParam): Promise<IPreSignedUrlPost>;
}
