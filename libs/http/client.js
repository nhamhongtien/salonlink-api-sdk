"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
class ApiError extends Error {
    constructor(message, code) {
        super(message);
        this.message = message;
        this.code = code;
    }
}
class ApiClient {
    static setApiConfig(apiConfig) {
        this.apiConfig = apiConfig;
    }
    static getApiConfig() {
        return this.apiConfig;
    }
    static setAuthenticator(authenticator) {
        this.authenticator = authenticator;
        return this;
    }
    static getAuthToken() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.authenticator) {
                return null;
            }
            return this.authenticator.getAuthToken();
        });
    }
    static success(response) {
        return response.data;
    }
    static error(e) {
        if (e.response && e.response.data) {
            const errorCode = e.response.data.code || 'unknown';
            const message = e.response.data.message || e.message;
            throw new ApiError(message, errorCode);
        }
        return this.handleUnknownError(e);
    }
    static handleUnknownError(e) {
        throw new ApiError(e.message, 'unknown');
    }
    static uri(uri) {
        return `${this.apiConfig.baseUrl}${uri}`;
    }
    static configs(params = {}, headers = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            const authHeader = yield this.authenticator.getAuthHeader();
            const baseUrl = this.apiConfig.baseUrl;
            if (authHeader !== '' && isAuthenticated) {
                headers = Object.assign(Object.assign({}, headers), { Authorization: authHeader });
            }
            headers = Object.assign(Object.assign({}, headers), { 'App-Version': this.apiConfig.appVersion, 'Device-Id': this.apiConfig.deviceId, 'User-Agent': this.apiConfig.userAgent, 'Client-Id': this.apiConfig.clientId });
            return { baseUrl, headers, params, withCredentials: false, crossdomain: true };
        });
    }
    static get(uri, params = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const config = yield this.configs(params, isAuthenticated);
                const r = yield axios_1.default.get(this.uri(uri), config);
                return this.success(r);
            }
            catch (e) {
                return this.error(e);
            }
        });
    }
    static post(uri, data = {}, params = {}, headers = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const config = yield this.configs(params, headers, isAuthenticated);
                const r = yield axios_1.default.post(this.uri(uri), data, config);
                return this.success(r);
            }
            catch (e) {
                this.error(e);
            }
        });
    }
    static patch(uri, data = {}, params = {}, headers = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const config = yield this.configs(params, headers, isAuthenticated);
                const r = yield axios_1.default.patch(this.uri(uri), data, config);
                return this.success(r);
            }
            catch (e) {
                this.error(e);
            }
        });
    }
    static put(uri, data = {}, params = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const config = yield this.configs(params, isAuthenticated);
                const r = yield axios_1.default.put(this.uri(uri), data, config);
                return this.success(r);
            }
            catch (e) {
                this.error(e);
            }
        });
    }
    static delete(uri, params = {}, isAuthenticated = true) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const config = yield this.configs(params, isAuthenticated);
                const r = yield axios_1.default.delete(this.uri(uri), config);
                return this.success(r);
            }
            catch (e) {
                this.error(e);
            }
        });
    }
    static upload(uri, params = {}, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const r = yield axios_1.default.post(uri, params, {
                    onUploadProgress: function (progressEvent) {
                        var loaded = progressEvent.loaded;
                        var total = progressEvent.total;
                        var percent = Math.round((loaded / total) * 100);
                        callback(percent);
                    }.bind(this)
                });
                return this.success(r);
            }
            catch (e) {
                this.error(e);
            }
        });
    }
}
exports.ApiClient = ApiClient;
