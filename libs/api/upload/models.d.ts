export interface IPreSignedUrlPost {
    url: string;
    fields: any;
}
export interface IUploadFile {
    id?: string;
    file_path: string;
}
