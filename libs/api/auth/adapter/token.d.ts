import { IAuthToken } from "../../../http/client";
import { AbstractAuthAdapter, ISocalAuth, SocialLoginParams } from './adapter';
export interface IToken {
    key: string;
}
export declare class TokenAuth extends AbstractAuthAdapter implements ISocalAuth {
    login(username: string, password: string): Promise<IToken>;
    getAuthToken(): Promise<IAuthToken | null>;
    refreshToken(): Promise<void>;
    facebookLogin(params: SocialLoginParams): Promise<IToken>;
    connectFacebook(params: SocialLoginParams): Promise<IToken>;
    googleLogin(params: SocialLoginParams): Promise<IToken>;
    twitterLogin(params: SocialLoginParams): Promise<IToken>;
    linkedInLogin(params: SocialLoginParams): Promise<IToken>;
}
