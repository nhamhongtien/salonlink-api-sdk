import { ApiClient } from '../../http/client';

export interface IStartupVersion {
    minimal_version: string;
    current_version: string;
    title: string;
    message?: string;
    icon?: string;
    background?: string;
    platform: string;
    app_store_url: string;
}

export interface IStartup {
    update: IStartupVersion;
}

export class Startup {
    static async get(): Promise<IStartup> {
        const uri = `/startup/`;
        return ApiClient.get(uri);
    }
}
